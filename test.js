module.exports = (function (){
  function word_to_u16(w) {
    var u = 0;
    for (var i = 0; i < 16; ++i) {
      u = u | (w._ === 'Word.i' ? 1 << i : 0);
      w = w.pred;
    };
    return u;
  };
  function u16_to_word(u) {
    var w = {_: 'Word.e'};
    for (var i = 0; i < 16; ++i) {
      w = {_: (u >>> (16-i-1)) & 1 ? 'Word.i' : 'Word.o', pred: w};
    };
    return w;
  };
  function u16_to_bits(x) {
    var s = '';
    for (var i = 0; i < 16; ++i) {
      s = (x & 1 ? '1' : '0') + s;
      x = x >>> 1;
    }
    return s;
  };
  const inst_unit = x=>x(null);
  const elim_unit = (x=>{var $1 = (()=>c0=>{var self = x;switch("unit"){case 'unit':var $0 = c0;return $0;};})();return $1;});
  const inst_bool = x=>x(true)(false);
  const elim_bool = (x=>{var $4 = (()=>c0=>c1=>{var self = x;if (self) {var $2 = c0;return $2;} else {var $3 = c1;return $3;};})();return $4;});
  const inst_nat = x=>x(0n)(x0=>1n+x0);
  const elim_nat = (x=>{var $8 = (()=>c0=>c1=>{var self = x;if (self===0n) {var $5 = c0;return $5;} else {var $6=(self-1n);var $7 = c1($6);return $7;};})();return $8;});
  const inst_bits = x=>x('')(x0=>x0+'0')(x0=>x0+'1');
  const elim_bits = (x=>{var $14 = (()=>c0=>c1=>c2=>{var self = x;switch(self.length===0?'e':self[self.length-1]==='0'?'o':'i'){case 'o':var $9=self.slice(0,-1);var $10 = c1($9);return $10;case 'i':var $11=self.slice(0,-1);var $12 = c2($11);return $12;case 'e':var $13 = c0;return $13;};})();return $14;});
  const inst_u16 = x=>x(x0=>word_to_u16(x0));
  const elim_u16 = (x=>{var $17 = (()=>c0=>{var self = x;switch('u16'){case 'u16':var $15=u16_to_word(self);var $16 = c0($15);return $16;};})();return $17;});
  const inst_string = x=>x('')(x0=>x1=>(String.fromCharCode(x0)+x1));
  const elim_string = (x=>{var $22 = (()=>c0=>c1=>{var self = x;if (self.length===0) {var $18 = c0;return $18;} else {var $19=self.charCodeAt(0);var $20=self.slice(1);var $21 = c1($19)($20);return $21;};})();return $22;});
  var run = (p) => {
    if (typeof window === 'undefined') {      var rl = eval("require('readline')").createInterface({input:process.stdin,output:process.stdout,terminal:false});
      var fs = eval("require('fs')");
      var pc = eval("process");
      var ht = eval("require('http')");
      var hs = eval("require('https')");
      var dg = eval("require('dgram')");
    } else {
      var rl = {question: (x,f) => f(''), close: () => {}};
      var fs = {readFileSync: () => ''};
      var pc = {exit: () => {}, argv: []};
      var ht = null;
      var hs = null;
      var dg = null;
    };
    var lib = {rl,fs,pc,ht,hs,dg};
    return run_io(lib,p)
      .then((x) => { rl.close(); return x; })
      .catch((e) => { rl.close(); throw e; });
  };
  var set_file = (lib, param) => {
    var path = '';
    for (var i = 0; i < param.length && param[i] !== '='; ++i) {
      path += param[i];
    };
    var data = param.slice(i+1);
    lib.fs.mkdirSync(path.split('/').slice(0,-1).join('/'),{recursive:true});
    lib.fs.writeFileSync(path,data);
    return '';
  };
  var del_file = (lib, param) => {
    try {
      lib.fs.unlinkSync(param);
      return '';
    } catch (e) {
      if (e.message.indexOf('EPERM') !== -1) {
        lib.fs.rmdirSync(param);
        return '';
      } else {
        throw e;
      }
    }
  };
  var get_file = (lib, param) => {
    return lib.fs.readFileSync(param, 'utf8');
  }
  var get_dir = (lib, param) => {
    return lib.fs.readdirSync(param).join(';');
  };
  var get_file_mtime = (lib, param) => {
    return String(lib.fs.statSync(param).mtime.getTime());
  };
  var request = (lib, param) => {
    if (typeof fetch === 'undefined') {
      return new Promise((res,err) => {
        (/^https/.test(param)?lib.hs:lib.ht).get(param, r => {
          let data = '';
          r.on('data', chunk => { data += chunk; });
          r.on('end', () => res(data));
        }).on('error', e => res(''));
      });
    } else {
      return fetch(param).then(res => res.text()).catch(e => '');
    }
  }
  let PORTS = {};
  function init_udp(lib, port_num) {
    return new Promise((resolve, reject) => {
      if (!PORTS[port_num]) {
        PORTS[port_num] = {socket: lib.dg.createSocket('udp4'), mailbox: []};
        PORTS[port_num].socket.bind(port_num);
        PORTS[port_num].socket.on('listening', () => resolve(PORTS[port_num]));
        PORTS[port_num].socket.on('message', (data, peer) => {
          var ip = peer.address;
          var port = peer.port;
          PORTS[port_num].mailbox.push({ip: peer.address, port: peer.port, data: data.toString('hex')});
        })
        PORTS[port_num].socket.on('error', (err) => {
          console.log('err');
          reject('UDP init error.');
        });
      } else {
        resolve(PORTS[port_num]);
      }
    });
  }
  async function send_udp(lib, port_num, to_ip, to_port_num, data) {
    var port = await init_udp(lib, port_num);
    port.socket.send(Buffer.from(data,'hex'), to_port_num, to_ip);
    return null;
  }
  async function recv_udp(lib, port_num) {
    var port = await init_udp(lib, port_num);
    var mailbox = port.mailbox;
    port.mailbox = [];
    return mailbox;
  }
  async function stop_udp(lib, port_num) {
    PORTS[port_num].socket.close();
    delete PORTS[port_num];
  }
  var file_error = e => {
    if (e.message.indexOf('NOENT') !== -1) {
      return '';
    } else {
      throw e;
    }
  };
  var io_action = {
    print: async (lib, param) => {
      console.log(param);
      return '';
    },
    put_string: async (lib, param) => {
      process.stdout.write(param);
      return '';
    },
    get_file: async (lib, param) => {
      try {
        return get_file(lib, param);
      } catch (e) {
        return file_error(e);
      }
    },
    set_file: async (lib, param) => {
      try {
        return set_file(lib, param)
      } catch (e) {
        return file_error(e);
      }
    },
    del_file: async (lib, param) => {
      try {
        return del_file(lib, param);
      } catch (e) {
        return file_error(e);
      }
    },
    get_dir: async (lib, param) => {
      try {
        return get_dir(lib, param);
      } catch (e) {
        return file_error(e);
      }
    },
    get_file_mtime: async (lib, param) => {
      try {
        return get_file_mtime(lib, param);
      } catch (e) {
        return file_error(e);
      }
    },
    get_time: async (lib, param) => {
      return String(Date.now());
    },
    exit: async (lib, param) => {
      lib.pc.exit();
      return '';
    },
    request: async (lib, param) => {
      return request(lib, param);
    },
    get_time: async (lib, param) => {
      return String(Date.now());
    },
    get_line: async (lib, param) => {
      return await new Promise((res,err) => {
        lib.rl.question(param, (line) => res(line));
      });
    },
    get_args: async (lib, param) => {
      return lib.pc.argv[2] || '';
    },
    init_udp: async (lib, param) => {
      try {
        await init_udp(lib, Number(param));
        return '';
      } catch (e) {
        return '';
      }
    },
    send_udp: async (lib, param) => {
      let [port_num, to_ip, to_port_num, data] = param.split(';');
      await send_udp(lib, Number(port_num), to_ip, Number(to_port_num), data);
      return '';
    },
    recv_udp: async (lib, param) => {
      var mailbox = await recv_udp(lib, Number(param));
      var reply = mailbox.map(x => x.ip + ',' + x.port + ',' + x.data).join(';');
      return reply;
    },
    stop_udp: async (lib, param) => {
      await stop_udp(lib, Number(param));
      return '';
    },
    sleep: async (lib, param) => {
      return await new Promise((resolve,reject) => {
        setTimeout(() => resolve(''), Number(param));
      });
    },
  };
  var run_io = async (lib, io, depth = 0) => {
    switch (io._) {
      case 'IO.end':
        return Promise.resolve(io.value);
      case 'IO.ask':
        var action = io_action[io.query];
        var answer = await action(lib, io.param);
        return await run_io(lib, io.then(answer), depth + 1);
      }
  };
  function IO$(_A$1){var $23 = null;return $23;};
  const IO = x0=>IO$(x0);
  function IO$ask$(_query$2,_param$3,_then$4){var $24 = ({_:'IO.ask','query':_query$2,'param':_param$3,'then':_then$4});return $24;};
  const IO$ask = x0=>x1=>x2=>IO$ask$(x0,x1,x2);
  function IO$bind$(_a$3,_f$4){var self = _a$3;switch(self._){case 'IO.end':var $26=self.value;var $27 = _f$4($26);var $25 = $27;break;case 'IO.ask':var $28=self.query;var $29=self.param;var $30=self.then;var $31 = IO$ask$($28,$29,(_x$8=>{var $32 = IO$bind$($30(_x$8),_f$4);return $32;}));var $25 = $31;break;};return $25;};
  const IO$bind = x0=>x1=>IO$bind$(x0,x1);
  function IO$end$(_value$2){var $33 = ({_:'IO.end','value':_value$2});return $33;};
  const IO$end = x0=>IO$end$(x0);
  function IO$monad$(_new$2){var $34 = _new$2(IO$bind)(IO$end);return $34;};
  const IO$monad = x0=>IO$monad$(x0);
  const Unit$new = null;
  function IO$put_string$(_text$1){var $35 = IO$ask$("put_string",_text$1,(_skip$2=>{var $36 = IO$end$(Unit$new);return $36;}));return $35;};
  const IO$put_string = x0=>IO$put_string$(x0);
  function String$cons$(_head$1,_tail$2){var $37 = (String.fromCharCode(_head$1)+_tail$2);return $37;};
  const String$cons = x0=>x1=>String$cons$(x0,x1);
  const String$concat = a0=>a1=>(a0+a1);
  function IO$print$(_text$1){var $38 = IO$put_string$((_text$1+"\u{a}"));return $38;};
  const IO$print = x0=>IO$print$(x0);
  function List$(_A$1){var $39 = null;return $39;};
  const List = x0=>List$(x0);
  const List$nil = ({_:'List.nil'});
  function List$cons$(_head$2,_tail$3){var $40 = ({_:'List.cons','head':_head$2,'tail':_tail$3});return $40;};
  const List$cons = x0=>x1=>List$cons$(x0,x1);
  function List$filter$(_f$2,_xs$3){var self = _xs$3;switch(self._){case 'List.cons':var $42=self.head;var $43=self.tail;var self = _f$2($42);if (self) {var $45 = List$cons$($42,List$filter$(_f$2,$43));var $44 = $45;} else {var $46 = List$filter$(_f$2,$43);var $44 = $46;};var $41 = $44;break;case 'List.nil':var $47 = List$nil;var $41 = $47;break;};return $41;};
  const List$filter = x0=>x1=>List$filter$(x0,x1);
  const Bool$false = false;
  const Bool$true = true;
  const Nat$ltn = a0=>a1=>(a0<a1);
  const Nat$gte = a0=>a1=>(a0>=a1);
  function List$concat$(_as$2,_bs$3){var self = _as$2;switch(self._){case 'List.cons':var $49=self.head;var $50=self.tail;var $51 = List$cons$($49,List$concat$($50,_bs$3));var $48 = $51;break;case 'List.nil':var $52 = _bs$3;var $48 = $52;break;};return $48;};
  const List$concat = x0=>x1=>List$concat$(x0,x1);
  function Main$qs$(_list$1){var self = _list$1;switch(self._){case 'List.cons':var $54=self.head;var $55=self.tail;var _fst$4 = $54;var _min$5 = List$filter$((_x$5=>{var $57 = (_x$5<$54);return $57;}),$55);var _max$6 = List$filter$((_x$6=>{var $58 = (_x$6>=$54);return $58;}),$55);var $56 = List$concat$(Main$qs$(_min$5),List$concat$(List$cons$(_fst$4,List$nil),Main$qs$(_max$6)));var $53 = $56;break;case 'List.nil':var $59 = List$nil;var $53 = $59;break;};return $53;};
  const Main$qs = x0=>Main$qs$(x0);
  function List$foldl$(_nil$3,_cons$4,_list$5){var List$foldl$=(_nil$3,_cons$4,_list$5)=>({ctr:'TCO',arg:[_nil$3,_cons$4,_list$5]});var List$foldl=_nil$3=>_cons$4=>_list$5=>List$foldl$(_nil$3,_cons$4,_list$5);var arg=[_nil$3,_cons$4,_list$5];while(true){let [_nil$3,_cons$4,_list$5]=arg;var R=(()=>{var self = _list$5;switch(self._){case 'List.cons':var $60=self.head;var $61=self.tail;var $62 = List$foldl$(_cons$4($60)(_nil$3),_cons$4,$61);return $62;case 'List.nil':var $63 = _nil$3;return $63;};})();if(R.ctr==='TCO')arg=R.arg;else return R;}};
  const List$foldl = x0=>x1=>x2=>List$foldl$(x0,x1,x2);
  function List$fold$(_list$2,_nil$4,_cons$5){var self = _list$2;switch(self._){case 'List.cons':var $65=self.head;var $66=self.tail;var $67 = _cons$5($65)(List$fold$($66,_nil$4,_cons$5));var $64 = $67;break;case 'List.nil':var $68 = _nil$4;var $64 = $68;break;};return $64;};
  const List$fold = x0=>x1=>x2=>List$fold$(x0,x1,x2);
  const Nat$lte = a0=>a1=>(a0<=a1);
  function Pair$(_A$1,_B$2){var $69 = null;return $69;};
  const Pair = x0=>x1=>Pair$(x0,x1);
  function Nat$succ$(_pred$1){var $70 = 1n+_pred$1;return $70;};
  const Nat$succ = x0=>Nat$succ$(x0);
  const Nat$sub = a0=>a1=>(a0-a1<=0n?0n:a0-a1);
  function Pair$new$(_fst$3,_snd$4){var $71 = ({_:'Pair.new','fst':_fst$3,'snd':_snd$4});return $71;};
  const Pair$new = x0=>x1=>Pair$new$(x0,x1);
  function Nat$div_mod$go$(_n$1,_d$2,_r$3){var Nat$div_mod$go$=(_n$1,_d$2,_r$3)=>({ctr:'TCO',arg:[_n$1,_d$2,_r$3]});var Nat$div_mod$go=_n$1=>_d$2=>_r$3=>Nat$div_mod$go$(_n$1,_d$2,_r$3);var arg=[_n$1,_d$2,_r$3];while(true){let [_n$1,_d$2,_r$3]=arg;var R=(()=>{var self = (_n$1<=_r$3);if (self) {var $72 = Nat$div_mod$go$(_n$1,Nat$succ$(_d$2),(_r$3-_n$1<=0n?0n:_r$3-_n$1));return $72;} else {var $73 = Pair$new$(_d$2,_r$3);return $73;};})();if(R.ctr==='TCO')arg=R.arg;else return R;}};
  const Nat$div_mod$go = x0=>x1=>x2=>Nat$div_mod$go$(x0,x1,x2);
  const Nat$div_mod = a0=>a1=>(({_:'Pair.new','fst':a0/a1,'snd':a0%a1}));
  function Nat$to_base$go$(_base$1,_nat$2,_res$3){var Nat$to_base$go$=(_base$1,_nat$2,_res$3)=>({ctr:'TCO',arg:[_base$1,_nat$2,_res$3]});var Nat$to_base$go=_base$1=>_nat$2=>_res$3=>Nat$to_base$go$(_base$1,_nat$2,_res$3);var arg=[_base$1,_nat$2,_res$3];while(true){let [_base$1,_nat$2,_res$3]=arg;var R=(()=>{var self = (({_:'Pair.new','fst':_nat$2/_base$1,'snd':_nat$2%_base$1}));switch(self._){case 'Pair.new':var $74=self.fst;var $75=self.snd;var self = $74;if (self===0n) {var $77 = List$cons$($75,_res$3);var $76 = $77;} else {var $78=(self-1n);var $79 = Nat$to_base$go$(_base$1,$74,List$cons$($75,_res$3));var $76 = $79;};return $76;};})();if(R.ctr==='TCO')arg=R.arg;else return R;}};
  const Nat$to_base$go = x0=>x1=>x2=>Nat$to_base$go$(x0,x1,x2);
  function Nat$to_base$(_base$1,_nat$2){var $80 = Nat$to_base$go$(_base$1,_nat$2,List$nil);return $80;};
  const Nat$to_base = x0=>x1=>Nat$to_base$(x0,x1);
  const String$nil = '';
  function Pair$snd$(_pair$3){var self = _pair$3;switch(self._){case 'Pair.new':var $82=self.snd;var $83 = $82;var $81 = $83;break;};return $81;};
  const Pair$snd = x0=>Pair$snd$(x0);
  const Nat$mod = a0=>a1=>(a0%a1);
  const Bool$and = a0=>a1=>(a0&&a1);
  const Nat$gtn = a0=>a1=>(a0>a1);
  function Maybe$(_A$1){var $84 = null;return $84;};
  const Maybe = x0=>Maybe$(x0);
  const Maybe$none = ({_:'Maybe.none'});
  function Maybe$some$(_value$2){var $85 = ({_:'Maybe.some','value':_value$2});return $85;};
  const Maybe$some = x0=>Maybe$some$(x0);
  function List$at$(_index$2,_list$3){var List$at$=(_index$2,_list$3)=>({ctr:'TCO',arg:[_index$2,_list$3]});var List$at=_index$2=>_list$3=>List$at$(_index$2,_list$3);var arg=[_index$2,_list$3];while(true){let [_index$2,_list$3]=arg;var R=(()=>{var self = _list$3;switch(self._){case 'List.cons':var $86=self.head;var $87=self.tail;var self = _index$2;if (self===0n) {var $89 = Maybe$some$($86);var $88 = $89;} else {var $90=(self-1n);var $91 = List$at$($90,$87);var $88 = $91;};return $88;case 'List.nil':var $92 = Maybe$none;return $92;};})();if(R.ctr==='TCO')arg=R.arg;else return R;}};
  const List$at = x0=>x1=>List$at$(x0,x1);
  function Nat$show_digit$(_base$1,_n$2){var _m$3 = (_n$2%_base$1);var _base64$4 = List$cons$(48,List$cons$(49,List$cons$(50,List$cons$(51,List$cons$(52,List$cons$(53,List$cons$(54,List$cons$(55,List$cons$(56,List$cons$(57,List$cons$(97,List$cons$(98,List$cons$(99,List$cons$(100,List$cons$(101,List$cons$(102,List$cons$(103,List$cons$(104,List$cons$(105,List$cons$(106,List$cons$(107,List$cons$(108,List$cons$(109,List$cons$(110,List$cons$(111,List$cons$(112,List$cons$(113,List$cons$(114,List$cons$(115,List$cons$(116,List$cons$(117,List$cons$(118,List$cons$(119,List$cons$(120,List$cons$(121,List$cons$(122,List$cons$(65,List$cons$(66,List$cons$(67,List$cons$(68,List$cons$(69,List$cons$(70,List$cons$(71,List$cons$(72,List$cons$(73,List$cons$(74,List$cons$(75,List$cons$(76,List$cons$(77,List$cons$(78,List$cons$(79,List$cons$(80,List$cons$(81,List$cons$(82,List$cons$(83,List$cons$(84,List$cons$(85,List$cons$(86,List$cons$(87,List$cons$(88,List$cons$(89,List$cons$(90,List$cons$(43,List$cons$(47,List$nil))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));var self = ((_base$1>0n)&&(_base$1<=64n));if (self) {var self = List$at$(_m$3,_base64$4);switch(self._){case 'Maybe.some':var $95=self.value;var $96 = $95;var $94 = $96;break;case 'Maybe.none':var $97 = 35;var $94 = $97;break;};var $93 = $94;} else {var $98 = 35;var $93 = $98;};return $93;};
  const Nat$show_digit = x0=>x1=>Nat$show_digit$(x0,x1);
  function Nat$to_string_base$(_base$1,_nat$2){var $99 = List$fold$(Nat$to_base$(_base$1,_nat$2),String$nil,(_n$3=>_str$4=>{var $100 = String$cons$(Nat$show_digit$(_base$1,_n$3),_str$4);return $100;}));return $99;};
  const Nat$to_string_base = x0=>x1=>Nat$to_string_base$(x0,x1);
  function Main$printList$(_list$1){var $101 = List$foldl$("",(_number$2=>_result$3=>{var _str$4 = Nat$to_string_base$(10n,_number$2);var $102 = (_result$3+_str$4);return $102;}),_list$1);return $101;};
  const Main$printList = x0=>Main$printList$(x0);
  const Main = IO$monad$((_m$bind$1=>_m$pure$2=>{var $103 = _m$bind$1;return $103;}))(IO$print$("QUICK SORT"))((_$1=>{var _sortedArray$2 = Main$qs$(List$cons$(1n,List$cons$(5n,List$cons$(2n,List$nil))));var $104 = IO$print$(Main$printList$(_sortedArray$2));return $104;}));
  return {
    '$main$': ()=>run(Main),
    'run': run,
    'IO': IO,
    'IO.ask': IO$ask,
    'IO.bind': IO$bind,
    'IO.end': IO$end,
    'IO.monad': IO$monad,
    'Unit.new': Unit$new,
    'IO.put_string': IO$put_string,
    'String.cons': String$cons,
    'String.concat': String$concat,
    'IO.print': IO$print,
    'List': List,
    'List.nil': List$nil,
    'List.cons': List$cons,
    'List.filter': List$filter,
    'Bool.false': Bool$false,
    'Bool.true': Bool$true,
    'Nat.ltn': Nat$ltn,
    'Nat.gte': Nat$gte,
    'List.concat': List$concat,
    'Main.qs': Main$qs,
    'List.foldl': List$foldl,
    'List.fold': List$fold,
    'Nat.lte': Nat$lte,
    'Pair': Pair,
    'Nat.succ': Nat$succ,
    'Nat.sub': Nat$sub,
    'Pair.new': Pair$new,
    'Nat.div_mod.go': Nat$div_mod$go,
    'Nat.div_mod': Nat$div_mod,
    'Nat.to_base.go': Nat$to_base$go,
    'Nat.to_base': Nat$to_base,
    'String.nil': String$nil,
    'Pair.snd': Pair$snd,
    'Nat.mod': Nat$mod,
    'Bool.and': Bool$and,
    'Nat.gtn': Nat$gtn,
    'Maybe': Maybe,
    'Maybe.none': Maybe$none,
    'Maybe.some': Maybe$some,
    'List.at': List$at,
    'Nat.show_digit': Nat$show_digit,
    'Nat.to_string_base': Nat$to_string_base,
    'Main.printList': Main$printList,
    'Main': Main,
  };
})();
module.exports['$main$']();
